Online Workshops 
----------------
A set of workshops you can host online, just fork and build for auto generated page.

Example can be found here: [http://eschabell.gitlab.io/openshift-preso-workshop](http://eschabell.gitlab.io/openshift-preso-workshop)

![Cover Slide](cover.png)
